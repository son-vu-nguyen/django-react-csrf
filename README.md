# django-react-csrf

This project aims to provide explanation on CSRF protection mechanism, and code sample on Django-React stack.

## Main concept

### 1. CSRF attack
   See full explanation [here](https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF))  
   Brief example: [(reference)](https://stackoverflow.com/questions/5207160/what-is-a-csrf-token-what-is-its-importance-and-how-does-it-work)  
   >* Assume you are currently logged into your online banking at `www.mybank.com`  
   >* Assume a money transfer from `mybank.com` will result in a request of (conceptually) the form `http://www.mybank.com/transfer?to=<SomeAccountnumber>;amount=<SomeAmount>`. (Your account number is not needed, because it is implied by your login.)  
   >* You visit `www.hacking.org`, not knowing that it is a malicious site.  
   >* If the owner of that site knows the form of the above request (easy!) and correctly guesses you are logged into `mybank.com` (requires some luck!), they could include on their page a request like `http://www.mybank.com/transfer?to=123456;amount=10000` (where 123456 is the number of their Cayman Islands account and 10000 is an amount that you previously thought you were glad to possess).
   >* You retrieved that `www.hacking.org` page, so your browser will make that request.  
   >* Your bank cannot recognize this origin of the request: Your web browser will send the request along with your `www.mybank.com` cookie and it will look perfectly legitimate. There goes your money!

### 2. Web Security Concepts

#### CORS policy (same-origin policy)
   Two pages have the same origin if the protocol, port (if one is specified), and host are the same for both pages. Typically, accessing resources from different origins are not allowed. Web clients from an origin can perform successful API calls to any different origins, but browsers will prevent that origin (specified in `Origin` header) from accessing the obtained resources if no `Access-Control-Allow-Origin` header specified, or that origin is not included in that header value.
   To grant access for other origins, target origin need to config in server-side to allow those origins by adding `Access-Control-Allow-Origin` header to the response.

#### Cookie policy
   Cookie is private data of user on a domain, then browsers protect it from being read or written by scripts of other domains.

#### Note
   In general, using one of these two policies can provide a good enough protection from CSRF attack. However, combination should bring better result, since attackers can make fake `Origin` header, or somehow bypass the Cookie policy. Be aware that browsers are not always reliable at all times (like bugs haha :D).

## CSRF Protection In Practice

#### Server-side rendering
   When rendering form, server generates a secure random token, then set it either in cookie and form (as a hidden field). Then when client submits form, server will validate these two tokens, one in cookie and one in data sent within the form. Since attackers cannot access other domain's cookie, they cannot guess the token should be sent in POST form, then no way to perform attack.

#### Client-side rendering

   For native apps, or web clients which host at same origins with API servers, they can generate their own tokens then set it both in cookie and POST form data.

   For web clients which host at different origins with API servers, because there is no way to set cookie for any other domains, then they cannot generate tokens at client-side. It should be done in server-side. Server should serve an API in order to set token to its domain cookie. Therefore, web clients can easily perform API calls which have cookie included in request.

## Django CSRF Protecion

   For server-side rendering form, Django server will set a `csrftoken` in cookie, and a hidden field `csrfmiddlewaretoken`, both in default length of 64 characters. These tokens are not equal since the truly `secret` has been salted. For validation, Django server will firstly unsalt these tokens before making comparation.

   If these tokens are equal, it must be a true match when running the same unsalt algorithm. For client-side rendering form, web client can easily generate its own token and set to cookie, or let server-side do it, then finally read that token from cookie and include it in POST form data.  
   If web client need server-side to set token, Django server should be in charge. This method decorator `ensure_csrf_cookie` will force a API view to send the CSRF cookie in response.  
   Then web client can perform POST API call with token embedded in both cookie and form data to server.
   
   Besides, for convenience, Django allow web client to set token in an alternative header `X-CSRFToken` instead of including it in `csrfmiddlewaretoken` field of form data.

   In order to let web clients from other origins call APIs, Django server need some configuration to allow CORS. Please refer to [this](https://github.com/ottoyiu/django-cors-headers) for more details.

## Developing Web Client With ReactJS

### Client and server host in same origin
   In this scenario, development and production stage are quite different. When in development, developers usually config a Webpack dev server on localhost to serve a React application for fast view and easy debugging. Then it will make the client origin become different with API server origin. This might violate CORS policy and need some configuration on API server just to support development stage, which is unnecessary in production.  
   ![Alt text](https://bitbucket.org/son-vu-nguyen/django-react-csrf/raw/d7c8c7f9107c69c7e288371ffff286d757e8e05c/assets/scenario1.PNG "Client and server host in same origin - development stage")

   To overcome this issue, web client should always call API to its own origin (which is true in production), and config Webpack dev server (web client's origin in development stage) to proxy all requests to the corresponding API server.  
   ![Alt text](https://bitbucket.org/son-vu-nguyen/django-react-csrf/raw/d7c8c7f9107c69c7e288371ffff286d757e8e05c/assets/scenario2.PNG "Client and server host in same origin - development stage")

   If you are using `create-react-app` to initialize your React application, just add this to `package.json`. This is an useful config to support proxy requests from Webpack dev server to API server.

    "proxy": "http://localhost:3001/"

### Client and server host in different origin
   Need to provide some necessary configs when fetching CORS requests.

#### Using [`axios`](https://github.com/axios/axios) library
    axios({
        method: "get",
        url: "/get/csrf/",
        baseURL: baseApiUrl,
        withCredentials: true
    });

    axios({
        method: "post",
        url: "...",
        data: {},
        headers: {
            "X-CSRFToken": csrfToken
        },
        baseURL: baseApiUrl,
        withCredentials: true
    });

   By default, `axios` sends user's cookie to server in non-CORS requests. It also allows CORS requests to other domain but does not automatically send cookie of that domain along with requests. `withCredentials` is used to indicates whether or not CORS requests should be made using credentials.

#### Using [`fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) API
    fetch(addUrlPrefix("/get/csrf/"), {
        method: "GET",
        mode: "cors",
        credentials: "include"
    });

    fetch(addUrlPrefix("..."), {
        method: "POST",
        body: JSON.stringify({}),
        headers: new Headers({
            "X-CSRFToken": csrfToken
        }),
        mode: "cors",
        credentials: "include"
    });

   [`mode`](https://developer.mozilla.org/en-US/docs/Web/API/Request/mode) is used to determine whether or not this request is CORS request.  
   [`credentials`](https://developer.mozilla.org/en-US/docs/Web/API/Request/credentials) is used to determine whether the user agent should send cookies from the other domain in the case of cross-origin requests.